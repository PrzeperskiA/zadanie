package org.antek.wypozyczalnia.controllers;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class MainPageController {
    @RequestMapping("/stronaglowna")
    public String strona() {
        return "Strona główna";
    }
}