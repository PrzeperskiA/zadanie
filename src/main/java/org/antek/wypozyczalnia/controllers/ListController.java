package org.antek.wypozyczalnia.controllers;

import org.antek.wypozyczalnia.ListMain;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class ListController {
    @RequestMapping("/list")
    public String lista() {
        ListMain list = new ListMain();
        return list.toString();
    }
}