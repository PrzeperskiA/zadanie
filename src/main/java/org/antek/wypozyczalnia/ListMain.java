package org.antek.wypozyczalnia;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;


public class ListMain {
    public static int zwracanie(String[] args) throws IOException {
        String filePath = "C:\\Users\\Antoni\\Desktop\\Lista.txt";
        int number = 0;
        BufferedReader fileReader = null;

        try {
            fileReader = new BufferedReader(new FileReader(filePath));
            String numberAsString = fileReader.readLine();
            number = Integer.parseInt(numberAsString);
        } finally {
            if (fileReader != null) {
                fileReader.close();
            }
        }
        return number;
    }
}